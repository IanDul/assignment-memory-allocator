#ifndef TESTS
#define TESTS

#include <stdbool.h>

void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void run_all_tests(void);

#endif
