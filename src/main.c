#include "test.h"
#include "mem.h"
#include <stdio.h>

#define HEAP_SIZE 16000

int main(void) {
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    run_all_tests();
    return 0;
}
