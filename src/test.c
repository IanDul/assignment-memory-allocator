#define _DEFAULT_SOURCE

#include "test.h"
#include "mem.h"
#include "util.h"
#include "mem_internals.h"

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

void test_1(void) {
    printf("-----TEST 1-----\n\n");
    void* mem = _malloc(100);
    struct block_header* block = block_get_header(mem);
    if (block && block->capacity.bytes == 100 && !block->is_free) {
        fprintf(stdout, "TEST FINISHED SUCCESSFULLY\n");
    } 
    else {
        fprintf(stderr, "TEST FAILED\n");
    }

    _free(mem);
    printf("----------------\n\n\n\n");
}

void test_2(void) {
    printf("-----TEST 2-----\n\n");
    void* mem1 = _malloc(100);
    void* mem2 = _malloc(100);
    void* mem3 = _malloc(100);
    struct block_header* block = block_get_header(mem1);
    _free(mem1);
    if (block->is_free && block && block->capacity.bytes == 100) {
        fprintf(stdout, "TEST FINISHED SUCCESSFULLY\n");
    } 
    else {
        fprintf(stderr, "TEST FAILED\n");
    }

    _free(mem2);
    _free(mem3);   
    printf("----------------\n\n\n\n");
}

void test_3(void) {
    printf("-----TEST 3-----\n\n");
    void* mem1 = _malloc(100);
    void* mem2 = _malloc(1000);
    void* mem3 = _malloc(100);
    struct block_header* block1 = block_get_header(mem1);
    struct block_header* block2 = block_get_header(mem2);
    _free(mem1);
    _free(mem2);
    if (block1 && block2 && block1->is_free && block2->is_free) {
        fprintf(stdout, "TEST FINISHED SUCCESSFULLY\n");
    } 
    else {
        fprintf(stderr, "TEST FAILED\n");
    }

    _free(mem3);
    printf("----------------\n\n\n\n");
}

void test_4(void) {
    printf("-----TEST 4-----\n\n");
    void* mem = _malloc(16500);
    struct block_header* block = block_get_header(mem);
    if (block && block->capacity.bytes == 16500 && !block->is_free) {
        fprintf(stdout, "TEST FINISHED SUCCESSFULLY\n");
    } 
    else {
        fprintf(stderr, "TEST FAILED\n");
    }
    _free(mem);
    printf("----------------\n\n\n\n");
}

void test_5(void) {
    printf("-----TEST 5-----\n\n");
    void* mem1 = _malloc(100);
    struct block_header* block1 = block_get_header(mem1);
    map_pages(block1->contents + block1->capacity.bytes, REGION_MIN_SIZE, 0);

    void* mem2 = _malloc(40000);
    struct block_header* block2 = block_get_header(mem2);
    if (block1->contents + block1->capacity.bytes == (void*) block2 && block1 && block2) {
        fprintf(stdout, "TEST FINISHED SUCCESSFULLY\n");
    } 
    else {
        fprintf(stderr, "TEST FAILED\n");
    }
    _free(mem1);
    _free(mem2);
    printf("----------------\n\n\n\n");
}

void run_all_tests(void) {
    printf("\n\n");
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}
